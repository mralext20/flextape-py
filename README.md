# Flextape.py

Flextape but it's python!

# Example/Usage

```python
from flextape import flextape
print(flextape.slap(["I sawed this ", "boat in half!"])) #  returns "I sawed this boat in half!"
print(flextape.slap(["I used ", 10, " coats of Flex Seal on my air boat!"])) #  returns "I used 10 coats of Flex Seal on my air boat!"
```

# Note

This was inspired by flextape-js by iknowbashfu (https://github.com/IKnowBashFu/flextape-js)